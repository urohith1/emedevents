*** Settings ***
Resource          ../../Global/setup.robot

*** Keywords ***
CreateConference
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //div[@id='myHeader']//li/a[@data-type='CONFERENCE']    ${long_wait}    Create a conference link is not visible after waiting ${long_wait}
    SeleniumLibrary.Click Element    //div[@id='myHeader']//li/a[@data-type='CONFERENCE']
    Comment    Add conference Title
    AddConferenceTitle    ${EMED_testdata}
    Comment    Add Conference Type information
    AddConferenceType    ${EMED_testdata}
    Comment    Add Conference start date information
    SelectStartDateAndTime    ${EMED_testdata}
    Comment    Add Conference end date
    SelectEndDateAndTime    ${EMED_testdata}
    Comment    Add multiple specialities
    AddSpecialities    ${EMED_testdata}
    Comment    Add Summary about conference
    AddSummary    ${EMED_testdata}
    Comment    Add Organiser information
    AddOrganiser    ${EMED_testdata}
    Comment    AddVenueDetails    ${EMED_testdata}
    Comment    Add Conference Url information
    AddConferenceUrl    ${EMED_testdata}
    Comment    Add Registration Url information
    AddRegistrationURL    ${EMED_testdata}
    Comment    Preview and publish the conference
    PreviewAndPublishConference    ${EMED_testdata}

AddSummary
    [Arguments]    ${EMED_testdata}
    Select Frame    //iframe[@id='summary_ifr']
    Wait Until Element Is Visible    //body[@id='tinymce']    ${long_wait}    Summary field is not visible after waiting ${long_wait}
    SeleniumLibrary.Input Text    //body[@id='tinymce']    ${EMED_testdata}[Summary]
    Unselect Frame

AddOrganiser
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //input[@placeholder='Choose an Organizer']    ${long_wait}    Organizer field is not visible after waiting ${long_wait}
    Input Text    //input[@placeholder='Choose an Organizer']    ${EMED_testdata}[OrganizerName]
    Wait Until Element Is Visible    //li[text()='${EMED_testdata}[OrganizerName]']    ${long_wait}    ${EMED_testdata}[OrganizerName] data is not visible after waiting ${long_wait}
    Press Keys    //li[text()='${EMED_testdata}[OrganizerName]']    ENTER

AddVenueDetails
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //input[@placeholder='Choose a Venue']    ${long_wait}    Venue Details are not visible after waiting ${long_wait}
    Input Text    //input[@placeholder='Choose a Venue']    ${EMED_testdata}[VenueDetails]
    Wait Until Element Is Visible    //li[text()='${EMED_testdata}[VenueDetails]']    ${long_wait}    ${EMED_testdata}[VenueDetails] details is not visible after waiting ${long_wait}
    Press Keys    //li[text()='${EMED_testdata}[VenueDetails]']    ENTER

AddConferenceUrl
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //input[@placeholder='Enter Conference URL']    ${long_wait}    Conference Url textbox is not visible after waiting ${long_wait}
    Input Text    //input[@placeholder='Enter Conference URL']    ${EMED_testdata}[ConferenceUrl]

AddRegistrationURL
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //input[@placeholder='Enter Registration URL']    ${long_wait}    Registration Url text box is not visible after waitng ${long_wait}
    Input Text    //input[@placeholder='Enter Registration URL']    ${EMED_testdata}[RegistrationUrl]

SelectStartDateAndTime
    [Arguments]    ${EMED_testdata}
    ${date}    Split String    ${EMED_testdata}[StartDate]    /
    LOG    ${date[0]}
    Wait Until Element Is Visible    //input[@id='startdate']    ${long_wait}    StartDate inputbox is not visible after waiting ${long_wait}
    SeleniumLibrary.Click Element    //input[@id='startdate']
    Wait Until Element Is Visible    //select[@data-handler='selectMonth']    ${long_wait}    Month dropdown is not visible after waiting ${long_wait}
    Select From List By Label    //select[@data-handler='selectMonth']    ${date[1]}
    Wait Until Element Is Visible    //select[@data-handler='selectYear']    ${long_wait}    Year dropdown is not visible after waiting ${long_wait}
    Select From List By Label    //select[@data-handler='selectYear']    ${date[2]}
    Wait Until Element Is Visible    //table[@class='ui-datepicker-calendar']//td/a[text()='${date[0]}']
    SeleniumLibrary.Click Element    //table[@class='ui-datepicker-calendar']//td/a[text()='${date[0]}']
    Wait Until Element Is Visible    //select[@id='start_time']    ${long_wait}    Start time select box is not visible after waiting ${long_wait}
    Select From List By Label    //select[@id='start_time']    ${EMED_testdata}[StartTime]

AddSpecialities
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //input[@placeholder='Choose specialties']    ${long_wait}    Specialities field is not visible after waiting ${long_wait}
    SeleniumLibrary.Input Text    //input[@placeholder='Choose specialties']    ${EMED_testdata}[Specialities]
    Wait Until Element Is Visible    //li[text()='${EMED_testdata}[Specialities]']    ${long_wait}    ${EMED_testdata}[Specialities] is not visible after waiting ${long_wait}
    Press Keys    //li[text()='${EMED_testdata}[Specialities]']    ENTER

AddConferenceType
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //select[@id='conference_type']    ${long_wait}    Conference Type field is not visible after waiting ${long_wait}
    Select From List By Label    //select[@id='conference_type']    ${EMED_testdata}[ConferenceType]

AddConferenceTitle
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //input[@id='title']    ${long_wait}    Conference title field is not visible after waiting ${long_wait}
    SeleniumLibrary.Input Text    //input[@id='title']    ${EMED_testdata}[ConferenceTitle]

SelectEndDateAndTime
    [Arguments]    ${EMED_testdata}
    ${end_date}    Split String    ${EMED_testdata}[EndDate]    /
    Wait Until Element Is Visible    //input[@id='enddate']    ${long_wait}    StartDate inputbox is not visible after waiting ${long_wait}
    SeleniumLibrary.Click Element    //input[@id='enddate']
    Wait Until Element Is Visible    //select[@data-handler='selectMonth']    ${long_wait}    Month dropdown is not visible after waiting ${long_wait}
    Select From List By Label    //select[@data-handler='selectMonth']    ${end_date}[1]
    Wait Until Element Is Visible    //select[@data-handler='selectYear']    ${long_wait}    Year dropdown is not visible after waiting ${long_wait}
    Select From List By Label    //select[@data-handler='selectYear']    ${end_date}[2]
    Wait Until Element Is Visible    //table[@class='ui-datepicker-calendar']//td/a[text()='${end_date}[0]']    ${long_wait}    Specified Date is not visible after waiting ${long_wait}
    SeleniumLibrary.Click Element    //table[@class='ui-datepicker-calendar']//td/a[text()='${end_date}[0]']
    Wait Until Element Is Visible    //select[@id='end_time']    ${long_wait}    End time select box is not visible after waiting ${long_wait}
    Select From List By Label    //select[@id='end_time']    ${EMED_testdata}[EndTime]

PreviewAndPublishConference
    [Arguments]    ${EMED_testdata}
    Wait Until Element Is Visible    //button[@id='savePreview']    ${long_wait}    Save and Preview button is not visible after waiting ${long_wait}
    Javascript Click By Xpath    //button[@id='savePreview']
    Wait Until Element Is Visible    //p[@id='conf_title']    ${long_wait}    Conference titile in preview page is not visible after waiting ${long_wait}
    ${conferenceTitle}    Get Text    //p[@id='conf_title']
    Should Be Equal    ${EMED_testdata}[ConferenceTitle]    ${conferenceTitle}    Created conference and expected conference are not same
    Wait Until Element Is Visible    //a[@id='publish_btn']    ${long_wait}    Publish button is not visible after waiting ${long_wait}
    Click Element    //a[@id='publish_btn']
    Wait Until Element Is Visible    //form[@id='better-rating-form']    ${long_wait}    Rating form is not visible after waiting ${long_wait}
    Click Element    //label[@class='star star-5']
    Wait Until Element Is Visible    //a[@id='give_feedback1']    ${long_wait}    Save and Publish button is not visible after waiting ${long_wait}
    click Element    //a[@id='give_feedback1']
    Sleep    5s
