*** Settings ***
Resource          ../../Global/setup.robot

*** Keywords ***
LaunchBrowserAndNavigateToUrl
    [Arguments]    ${browser_name}    ${url}
    Run Keyword If    '${browser_name}'=='chrome' or '${browser_name}'=='gc' or '${browser_name}'=='Chrome'    Open Chrome Browser    ${url}
    ...    ELSE    LaunchBrowserAndNavigateToUrl    ${browser_name}    ${url}

LoginToApplication
    [Arguments]    ${userId}
    LaunchBrowserAndNavigateToUrl    ${BrowserName}    ${url}
    Maximize Browser Window
    Wait Until Element Is Visible    //input[@type='email']    ${long_wait}    Email field is not visible after waiting ${long_wait}
    Input Text    //input[@type='email']    ${userId}
    Wait Until Element Is Visible    //button[@class='sign_Inbtn btn']    ${long_wait}    SignIn button is not visible after waiting ${long_wait}
    SeleniumLibrary.Click Element    //button[@class='sign_Inbtn btn']

ReadExcelData
    [Arguments]    ${filePath}    ${testcase_id}    ${sheet}
    ${EMED_testdata}    CustomLibrary.Get Ms Excel Row Values Into Dictionary Based On Key    ${filePath}    ${sheet}    ${testcase_id}
    ${rowcount}    Get Length    ${EMED_testdata}
    Run Keyword If    ${rowcount}==0    Fail    EMEDEVENTS TestData is not found.
    Set Test Variable    ${EMED_testdata}
