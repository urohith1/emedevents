from robot.libraries.BuiltIn import BuiltIn
import time
from selenium import webdriver
import calendar
from datetime import datetime, time, date
from datetime import datetime
from datetime import date
import os
import time
import xlrd
import traceback
from selenium.webdriver.common import keys
                
class CustomLibrary(object):

        def __init__(self):
                pass

        @property
        def _sel_lib(self):
            return BuiltIn().get_library_instance('SeleniumLibrary')

        @property
        def _driver(self):
            return self._sel_lib.driver
        
        def open_chrome_browser(self,url):
            selenium = BuiltIn().get_library_instance('SeleniumLibrary')
            try:
                options = webdriver.ChromeOptions()
                options.add_argument("disable-extensions")
                options.add_experimental_option("excludeSwitches",["enable-automation","load-extension"])
                selenium.create_webdriver('Chrome',chrome_options=options)
                selenium.go_to(url)
                return True
            except:
                return False

        def get_ms_excel_row_values_into_dictionary_based_on_key(self, filepath, keyName, sheetName=None):
            """Returns the dictionary of values given row in the MS Excel file """
            workbook = xlrd.open_workbook(filepath)
            snames = workbook.sheet_names()
            dictVar = {}
            if sheetName == None:
                sheetName = snames[0]
            if self.validate_the_sheet_in_ms_excel_file(filepath, sheetName) == False:
                return dictVar
            worksheet = workbook.sheet_by_name(sheetName)
            noofrows = worksheet.nrows
            dictVar = {}
            headersList = worksheet.row_values(int(0))
            for rowNo in range(1, int(noofrows)):
                rowValues = worksheet.row_values(int(rowNo))
                if str(rowValues[0]) != str(keyName):
                    continue
                for rowIndex in range(0, len(rowValues)):
                    cell_data = rowValues[rowIndex]
                    if (str(cell_data) == "" or str(cell_data) == None):
                        continue
                    cell_data=self.get_unique_test_data(cell_data)
                    dictVar[str(headersList[rowIndex])] = str(cell_data)
            return dictVar

        def validate_the_sheet_in_ms_excel_file(self, filepath, sheetName):
            """Returns the True if the specified work sheets exist in the specifed MS Excel file else False"""
            workbook = xlrd.open_workbook(filepath)
            snames = workbook.sheet_names()
            sStatus = False
            if sheetName == None:
                return True
            else:
                for sname in snames:
                    if sname.lower() == sheetName.lower():
                        wsname = sname
                        sStatus = True
                        break
                if sStatus == False:
                    print("Error: The specified sheet: " + str(
                        sheetName) + " doesn't exist in the specified file: " + str(filepath))
            return sStatus

        def get_unique_test_data(self, testdata):
            """Returns the unique if data contains unique word """
            faker = BuiltIn().get_library_instance('FakerLibrary')
            unique_string = faker.random_number(5, True)
            unique_string = str(unique_string)
            testdata = testdata.replace("UNIQUE", unique_string)
            testdata = testdata.replace("Unique", unique_string)
            testdata = testdata.replace("unique", unique_string)
            return testdata

        def javascript_click_by_xpath(self, xpath):
            element = self._driver.find_element_by_xpath(xpath)
            self._driver.execute_script("arguments[0].click();", element)

