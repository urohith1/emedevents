*** Settings ***
Resource          ../Global/setup.robot

*** Test Cases ***
1.Publish a conference
    [Documentation]    This Test Case is used to create a medical conference.
    [Setup]    ReadExcelData    ${EMEDEVENTS_DATA}    ConferenceCreation    TC_01
    Comment    Login into Application
    LoginToApplication    ${userName}
    Comment    Create and publish Conference
    CreateConference    ${EMED_testdata}
    [Teardown]    Close Browser
